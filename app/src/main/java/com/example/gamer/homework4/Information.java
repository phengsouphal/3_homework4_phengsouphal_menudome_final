package com.example.gamer.homework4;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

public class Information extends AppCompatActivity {


    Button register,pickimg;
    EditText studentname,studentphone,studentclassroom;
    ImageView imageView;
    public static final int PICK_IMAGE=100;

    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        register=findViewById(R.id.btnregis);

        studentname=findViewById(R.id.stuname);
        studentphone=findViewById(R.id.phone);
        studentclassroom=findViewById(R.id.stuclass);
        register=findViewById(R.id.btnregis);
        pickimg=findViewById(R.id.btnimg);
        imageView=findViewById(R.id.imgView);


        pickimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(Information.this,MainActivity.class);
                String name=studentname.getText().toString();
                String phone=studentphone.getText().toString();
                String classroom=studentclassroom.getText().toString();


                DetailInformation detailInfo=new DetailInformation(name,phone,classroom,imageUri);
                intent.putExtra("DETAIL",detailInfo);
                setResult(RESULT_OK,intent);


                finish();
            }
        });

    }

    private void openGallery(){
        Intent gallery=new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(gallery,PICK_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==RESULT_OK&&requestCode==PICK_IMAGE){
            imageUri=data.getData();
            imageView.setImageURI(imageUri);

        }


    }

}
