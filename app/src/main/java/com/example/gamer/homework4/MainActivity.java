package com.example.gamer.homework4;

import android.app.Activity;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView txtname,txtphone,txtclass;
    private  ImageView imgview;

    LinearLayout mparent;
    DetailInformation detailInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mparent=findViewById(R.id.mainlayout);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            //Add New Data
            case R.id.option1:
                Toast.makeText(this,"Add New",Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(this, Information.class);
                startActivityForResult(intent,2);
                return true;

            case R.id.option2:
                mparent.removeAllViews();


                Toast.makeText(this,"Remove",Toast.LENGTH_SHORT).show();
                return true;

            case R.id.option3:
                        imagedetail();

                return true;

            case R.id.submenu:
                return true;

                //Fade-in
            case R.id.sub1:

                    fadein();
                return true;

            //Fade-out
            case R.id.sub2:

                    fadeout();
                return true;

            //Zoom-in
            case R.id.sub3:

                    zoomin();
                return true;

            //Zoom-out
            case R.id.sub4:

                    zoomout();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode==2){
                if (resultCode==RESULT_OK){

                    mparent.removeAllViews();
                    View view=getLayoutInflater().inflate(R.layout.layout,null);

                    txtname=view.findViewById(R.id.txtstuname);
                    txtphone=view.findViewById(R.id.txtphone);
                    txtclass=view.findViewById(R.id.txtstuclass);
                    imgview=view.findViewById(R.id.imgView);

                    mparent.addView(view);

                    detailInfo= data.getParcelableExtra("DETAIL");
                    txtname.setText("Name:   "+detailInfo.getStuname());
                    txtphone.setText("Phone:   "+detailInfo.getPhone());
                    txtclass.setText("Class:   "+detailInfo.getStuclass());
                    imgview.setImageURI(detailInfo.getPicture());


                }
                else {
                    Toast.makeText(this,"Nothing Nothing",Toast.LENGTH_SHORT).show();
                }
        }


    }
        //Animation
    public void fadein(){
        Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fadein);
        imgview.startAnimation(animFadeIn);
    }
    public void fadeout(){
        Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fadeout);
        imgview.startAnimation(animFadeIn);
    }
    public void zoomin(){
        Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.zoonin);
        imgview.startAnimation(animFadeIn);
    }
    public void zoomout(){
        Animation animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.zoomout);
        imgview.startAnimation(animFadeIn);
    }

    //View Image
    public void imagedetail(){
        mparent.removeAllViews();
        View view=getLayoutInflater().inflate(R.layout.viewimage,null);
        ImageView img =view.findViewById(R.id.viewimg);
        mparent.addView(view);
        Toast.makeText(this,"Don't Forget to trun on your Permission"+detailInfo.getPicture(),Toast.LENGTH_SHORT).show();
        img.setImageURI(detailInfo.getPicture());

    }

}
