package com.example.gamer.homework4;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

public class DetailInformation implements Parcelable {

    private String stuname;
    private String phone;
    private String stuclass;
    private Uri picture;

    public DetailInformation(String stuname, String phone, String stuclass, Uri picture) {
        this.stuname = stuname;
        this.phone = phone;
        this.stuclass = stuclass;
        this.picture = picture;
    }

    protected DetailInformation(Parcel in) {
        stuname = in.readString();
        phone = in.readString();
        stuclass = in.readString();
        picture = in.readParcelable(Uri.class.getClassLoader());
    }

    public static final Creator<DetailInformation> CREATOR = new Creator<DetailInformation>() {
        @Override
        public DetailInformation createFromParcel(Parcel in) {
            return new DetailInformation(in);
        }

        @Override
        public DetailInformation[] newArray(int size) {
            return new DetailInformation[size];
        }
    };

    public String getStuname() {
        return stuname;
    }

    public void setStuname(String stuname) {
        this.stuname = stuname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getStuclass() {
        return stuclass;
    }

    public void setStuclass(String stuclass) {
        this.stuclass = stuclass;
    }

    public Uri getPicture() {
        return picture;
    }

    public void setPicture(Uri picture) {
        this.picture = picture;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(stuname);
        dest.writeString(phone);
        dest.writeString(stuclass);
        dest.writeParcelable(picture, flags);
    }
}
